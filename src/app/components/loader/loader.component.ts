import { Component, OnInit } from '@angular/core';
import { LoaderService } from '@app/services/loader/loader.service';

@Component(
{
    selector    : 'app-loader',
    templateUrl : './loader.component.html',
    styleUrls   : ['./loader.component.css']
})


export class LoaderComponent implements OnInit
{

    public loading: boolean = false;

    constructor(private LoaderService: LoaderService)
    {
        this.LoaderService.isLoading.subscribe((v: any) =>
        {
            if(v) this.loading = v;
            else  setTimeout(() => { this.loading = v; }, 2500);
        });
    }


    ngOnInit() { }

}
