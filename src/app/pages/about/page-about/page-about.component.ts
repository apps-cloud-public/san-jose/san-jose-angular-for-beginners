import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';


@Component(
{
    selector    : 'app-page-about',
    templateUrl : './page-about.component.html',
    styleUrls   : ['./page-about.component.css'],
    host        : { 'class' : 'ui-section' },
})


export class AboutComponent implements OnInit
{

  constructor(private http: HttpClient) { }

  ngOnInit(): void { }

}
