import { NgModule } from '@angular/core';
import { AboutRoutingModule } from './about-routing.module';
import { AboutComponent } from './page-about/page-about.component';


@NgModule(
{
    declarations : [AboutComponent,],
    imports      : [AboutRoutingModule],
    exports      : [],
    providers    : [],
    bootstrap    : []
})


export class AboutModule { }
