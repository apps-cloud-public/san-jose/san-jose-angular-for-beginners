import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';


@Component(
{
    selector    : 'app-page-blog',
    templateUrl : './page-blog.component.html',
    styleUrls   : ['./page-blog.component.css'],
    host        : { 'class' : 'ui-section' },
})


export class BlogComponent implements OnInit
{

  constructor(private http: HttpClient) { }

  ngOnInit(): void { }

}
