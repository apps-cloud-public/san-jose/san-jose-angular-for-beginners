import { NgModule } from '@angular/core';
import { BlogRoutingModule } from './blog-routing.module';
import { BlogComponent } from './page-blog/page-blog.component';


@NgModule(
{
    declarations : [BlogComponent,],
    imports      : [BlogRoutingModule],
    exports      : [],
    providers    : [],
    bootstrap    : []
})


export class BlogModule { }
