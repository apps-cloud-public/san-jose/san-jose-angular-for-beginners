import { NgModule } from '@angular/core';
import { HomeRoutingModule } from './home-routing.module';
import { HomeComponent } from './page-home/page-home.component';


@NgModule(
{
    declarations : [HomeComponent,],
    imports      : [HomeRoutingModule],
    exports      : [],
    providers    : [],
    bootstrap    : []
})


export class HomeModule { }
