import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LolComponent } from './page-lol/page-lol.component';


const routes: Routes =
[
    {
        path      : '',
        component :  LolComponent
    }
];


@NgModule({ imports: [RouterModule.forChild(routes)], exports: [RouterModule] })


export class LolRoutingModule { }
