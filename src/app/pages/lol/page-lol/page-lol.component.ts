import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';


@Component(
{
    selector    : 'app-page-lol',
    templateUrl : './page-lol.component.html',
    styleUrls   : ['./page-lol.component.css'],
    host        : { 'class' : 'ui-section' },
})


export class LolComponent implements OnInit
{

  constructor(private http: HttpClient) { }

  ngOnInit(): void { }

}
