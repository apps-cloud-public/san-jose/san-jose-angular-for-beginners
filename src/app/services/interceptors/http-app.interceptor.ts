import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, Observer } from 'rxjs';
import { LoaderService } from './../loader/loader.service';


@Injectable({ providedIn: 'root' })
export class HttpAppInterceptor implements HttpInterceptor
{
    private requests: HttpRequest<any>[] = [];

    constructor(private LoaderService: LoaderService) { }

    removeRequest(req: HttpRequest<any>)
    {
        const i = this.requests.indexOf(req);

        if (i >= 0) this.requests.splice(i, 1);

        this.LoaderService.isLoading.next(this.requests.length > 0);
    }


    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>>
    {
        this.requests.push(req);
        this.LoaderService.isLoading.next(true);


        return new Observable<any>((observer: Observer<any>)  =>
        {
            const subscription = next.handle(req).subscribe(

                event =>
            {
                if (event instanceof HttpResponse)
                {
                    this.removeRequest(req);
                    observer.next(event);
                }
            },

            err =>
            {
                alert('error' + err);
                this.removeRequest(req);
                observer.error(err);
            },

            () =>
            {
                this.removeRequest(req);
                observer.complete();
            });


            return () =>
            {
                this.removeRequest(req);
                subscription.unsubscribe();
            };
        });
    }
}
